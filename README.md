## Portfolio Project!

This is a portfolio website that I created using React, Redux, Semantic Ui, and Shards React. It tells you about myself as well as my work history. Enjoy!

### Began on Github

This project began on Github where most of the work was pushed to since this was this site, as well as Gitlab, were used during my time
at Kenzie Academy. However, Github was used more during my studies of Back End programming so I was not accustomed to using GH-Pages and
getting a website link created for others to see. After many attempts I could not get the site link to pull up anything aside from this
README.md page so I decided to move the repository over to Gitlab.

### Even more issues with Gitlab

Since Gitlab was used during my Front End studies, I was use to deploying a site to Gitlab with a link that leads to my finished project. However, I had never used it for a project that contained multiple pages. The projects that were submitted to Gitlab were single page projects. Again, after many trails and errors, I could not get the link to display the Resume Page of my portfolio. Frustrated and ready for the world to see my portfolio, I turned to the one thing I knew would work, Heroku.

### Finally Deployed with Heroku!

Heroku is a deploying site that my team(s) and I have used to deploy our bigger projects such as our Kwitter Project and our capstone projects, see the Resume Page of my portfolio for more info about them. I was finally able to deploy my project in its entirety to Heroku and now everything runs the way it should. I'm aware that attempting to deploy to three different sites is a little strange however I believe that this shows a sneak of my problem solving skills.